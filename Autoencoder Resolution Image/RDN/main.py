# import numpy as np
# from tensorflow.keras.initializers import RandomUniform
# import matplotlib.pyplot as plt
# import cv2
# # import l1_loss
# # from psnr_stitched import stitched_PSNR
# # from ssim_stitched import stitched_ssim
# import tensorflow as tf
# from tensorflow.keras import Model, Input
# from tensorflow.keras.layers import (
#     concatenate,
#     Concatenate,
#     Activation,
#     Conv2D,
#     UpSampling2D,
#     Add,
#     Lambda,
# )

# from tensorflow.keras.preprocessing import image
# from tqdm import tqdm
# import glob

# path = "C:\\Users\\HP\\akademi-ai\\Autoencoder Resolution Image\\RDN\\"

# train_images = glob.glob(path + "train/*.jpg")
# val_images = glob.glob(path + "test/*.jpg")

# print("Number of training images:", len(train_images))
# print("Number of validation images:", len(val_images))

# def read(path):
#     img = image.load_img(path, target_size=(32, 32, 3))
#     img = image.img_to_array(img)
#     img = img / 255.0
#     return img


# def readVal(path):
#     img = image.load_img(path, target_size=(64, 64, 3))
#     img = image.img_to_array(img)
#     img = img / 255.0
#     return img


# train_x = np.array([read(image) for image in tqdm(train_images)])
# val_x = np.array([readVal(image) for image in tqdm(val_images)])

# print("Training images shape:", train_x.shape)
# print("Validation images shape:", val_x.shape)

# plt.imshow(val_x[0])
# # plt.show()

# # now we will make input images by lowering resolution without changing the size
# def pixelate_image(image, scale_percent=100):
#     width = int(image.shape[1] * scale_percent / 100)
#     height = int(image.shape[0] * scale_percent / 100)
#     dim = (width, height)

#     small_image = cv2.resize(image, dim, interpolation=cv2.INTER_AREA)

#     # scale back to original size
#     width = int(small_image.shape[1] * 100 / scale_percent)
#     height = int(small_image.shape[0] * 100 / scale_percent)
#     dim = (width, height)

#     low_res_image = cv2.resize(small_image, dim, interpolation=cv2.INTER_AREA)

#     return low_res_image


# train_x_px = []

# for i in range(train_x.shape[0]):
#     temp = pixelate_image(train_x[i, :, :, :])
#     train_x_px.append(temp)

# train_x_px = np.array(train_x_px)


# # get low resolution images for the validation set
# val_x_px = []

# for i in range(val_x.shape[0]):
#     temp = pixelate_image(val_x[i, :, :, :])
#     val_x_px.append(temp)

# val_x_px = np.array(val_x_px)

# print("Training images pixelated shape:", train_x_px.shape)
# print("Validation images pixelated shape:", val_x_px.shape)

# plt.figure(figsize=(20, 10))

# plt.subplot(121)
# plt.title("400x400 non pixelated image")
# plt.imshow(train_x[20])

# plt.subplot(122)
# plt.title("400x400 pixelated image")
# plt.imshow(train_x_px[20])

# # plt.show()
# def feature_extract(input_tensor, filter, kernel):
#     """
#     Runs every input image seperately through the same Conv-Layer
#     and concatenates all tensors at the end
#     :param G0: filter size of the Convolutional layer
#     :param input_tensor: the input layer of the extractor
#     :return:
#     """
#     conv = Conv2D(filter, kernel_size=kernel, padding='same', activation='relu', name='input_feature_conv')
#     input_size = input_tensor.get_shape().as_list()
#     in_conv_list = []
#     index = 1
#     for i in range(0, input_size[3], 3):
#         x = Lambda(lambda x: x[:, :, :, i:i+3], name='img_{}'.format(str(index)))(input_tensor)
#         in_conv_list.append(conv(x))
#         index += 1

#     return Concatenate(axis=3, name='conc_img_features')(in_conv_list)
    
# def create_RDB(prior_layer, block_name, G0=64, G=32, C=6):
#     """
#     Creates one residual dense block
#     :param prior_layer: the layer that was there before the block
#     :param block_name: name of the RDB
#     :param G0: filtersize for the last convolutional layer
#     :param G: filtersize per convolutional layer
#     :param C: Amount of convolutional layers per RDB
#     :return: a residual dense block with C conv layers
#     """
#     layerlist = [prior_layer]
#     conv_layer = Conv2D(G, kernel_size=3, padding='same', activation='relu', name=block_name+'_conv1')(prior_layer)

#     # iterating from 2 on in order to have a clear labelling of the layers
#     for i in range(2, C + 1):
#         layerlist.append(conv_layer)
#         out = Concatenate(axis=3)(layerlist)  # concatenate the output over the color channel
#         conv_layer = Conv2D(G, kernel_size=3, padding='same', activation='relu', name=block_name+'_conv'+str(i))(out)

#     # append the last convolutional layer of the RDB to the rest of conv layers
#     layerlist.append(conv_layer)

#     out = Concatenate(axis=3, name=block_name+'_conc')(layerlist)

#     # the last conv layer which has a kernel_size of 1 as in the paper
#     feat = Conv2D(G0, kernel_size=1, padding='same', activation='relu', name=block_name+'_local_Conv')(out)

#     # the last skip connection
#     feat = Add()([feat, prior_layer])

#     return feat

# def depth_to_space(input_layer, blocksize):
#     """
#     implements the tensorflow depth to space function
#     """
#     return Lambda(lambda x: tf.depth_to_space(x, block_size=blocksize, data_format='NHWC'), name='Depth_to_Space',)(input_layer)


# G0=64
# G=32 
# D=20 
# C=6

# input_size = [64,64,3]

# inputs = Input(input_size)

# # extract features for every input image
# conv1 = feature_extract(inputs, 64, 3)

# # first RDB
# RDB = create_RDB(conv1, 'RDB1', G0, G, C)
# RDBlocks_list = [RDB, ]

# # add the remaining RDB
# for i in range(2, D + 1):
#     RDB = create_RDB(RDB, 'RDB' + str(i), G0, G, C)
#     RDBlocks_list.append(RDB)

# RDB_out = Concatenate(axis=3)(RDBlocks_list)
# RDB_out = Conv2D(G0, kernel_size=1, padding='same', name='global_1x1_conv')(RDB_out)
# RDB_out = Conv2D(G0, kernel_size=3, padding='same', name='global_conv3')(RDB_out)
# out = Add()([RDB_out, conv1])

# # concatenate the very first extracted features with the output of the residual learning
# out = Concatenate(axis=3)([out, conv1])

# # Upscaling / depth to space
# out = Conv2D(256, kernel_size=3, padding='same', name='upscale_conv_1')(out)
# out = Conv2D(128, kernel_size=3, padding='same', name='upscale_conv_2')(out)
# out = depth_to_space(out, 2)

# # since we output a color image, we want 3 filters as the last layer
# out = Conv2D(3, kernel_size=3, padding='same')(out)

# model = Model(inputs=inputs, outputs=out)
# model.compile(optimizer='adam',  metrics=['accuracy'])

# model.summary()

# # kernel_size = 3
# # init_extreme_val = 0.05
# # upscaling = "ups"
# # # {'C': 6, 'D': 20, 'G': 64, 'G0': 64, 'x': 2},

# # initializer = RandomUniform(
# #     minval=-init_extreme_val, maxval=init_extreme_val, seed=None
# # )


# # def _RDBs(input_layer):
# #     """RDBs blocks.
# #     Args:
# #         input_layer: input layer to the RDB blocks (e.g. the second convolutional layer F_0).
# #     Returns:
# #         concatenation of RDBs output feature maps with G0 feature maps.
# #     """
# #     rdb_concat = list()
# #     rdb_in = input_layer
# #     for d in range(1, 20 + 1):
# #         x = rdb_in
# #         for c in range(1, 6 + 1):
# #             F_dc = Conv2D(
# #                 64,
# #                 kernel_size=kernel_size,
# #                 padding="same",
# #                 kernel_initializer=initializer,
# #                 name="F_%d_%d" % (d, c),
# #             )(x)
# #             F_dc = Activation("relu", name="F_%d_%d_Relu" % (d, c))(F_dc)
# #             # concatenate input and output of ConvRelu block
# #             # x = [input_layer,F_11(input_layer),F_12([input_layer,F_11(input_layer)]), F_13..]
# #             x = concatenate([x, F_dc], axis=3, name="RDB_Concat_%d_%d" % (d, c))
# #         # 1x1 convolution (Local Feature Fusion)
# #         x = Conv2D(
# #             64, kernel_size=1, kernel_initializer=initializer, name="LFF_%d" % (d)
# #         )(x)
# #         # Local Residual Learning F_{i,LF} + F_{i-1}
# #         rdb_in = Add(name="LRL_%d" % (d))([x, rdb_in])
# #         rdb_concat.append(rdb_in)

# #     assert len(rdb_concat) == 20

# #     return concatenate(rdb_concat, axis=3, name="LRLs_Concat")


# # def _UPN(input_layer):
# #     """ Upscaling layers. With old weights use _upsampling_block instead of _pixel_shuffle. """

# #     x = Conv2D(
# #         64,
# #         kernel_size=5,
# #         strides=1,
# #         padding="same",
# #         name="UPN1",
# #         kernel_initializer=initializer,
# #     )(input_layer)
# #     x = Activation("relu", name="UPN1_Relu")(x)
# #     x = Conv2D(
# #         400, kernel_size=3, padding="same", name="UPN2", kernel_initializer=initializer
# #     )(x)
# #     x = Activation("relu", name="UPN2_Relu")(x)
# #     if upscaling == "shuffle":
# #         return _pixel_shuffle(x)
# #     elif upscaling == "ups":
# #         return _upsampling_block(x)
# #     else:
# #         raise ValueError("Invalid choice of upscaling layer.")


# # def _upsampling_block(input_layer):
# #     """ Upsampling block for old weights. """

# #     x = Conv2D(
# #         3 * 2 ** 2,
# #         kernel_size=3,
# #         padding="same",
# #         name="UPN3",
# #         kernel_initializer=initializer,
# #     )(input_layer)
# #     return UpSampling2D(size=2, name="UPsample")(x)


# # def _pixel_shuffle(input_layer):
# #     """ PixelShuffle implementation of the upscaling layer. """

# #     x = Conv2D(
# #         3 * 2 ** 2,
# #         kernel_size=3,
# #         padding="same",
# #         name="UPN3",
# #         kernel_initializer=initializer,
# #     )(input_layer)
# #     return Lambda(
# #         lambda x: tf.nn.depth_to_space(x, block_size=2, data_format="NHWC"),
# #         name="PixelShuffle",
# #     )(x)


# # LR_input = Input(shape=(32, 32, 3), name="LR")
# # print(LR_input.shape)

# # F_m1 = Conv2D(
# #     64,
# #     kernel_size=kernel_size,
# #     padding="same",
# #     kernel_initializer=initializer,
# #     name="F_m1",
# # )(LR_input)
# # print(F_m1.shape)

# # F_0 = Conv2D(
# #     64,
# #     kernel_size=kernel_size,
# #     padding="same",
# #     kernel_initializer=initializer,
# #     name="F_0",
# # )(F_m1)
# # print(F_0.shape)

# # FD = _RDBs(F_0)
# # print(FD.shape)

# # GFF1 = Conv2D(
# #     64, kernel_size=1, padding="same", kernel_initializer=initializer, name="GFF_1",
# # )(FD)
# # print(GFF1.shape)

# # GFF2 = Conv2D(
# #     64,
# #     kernel_size=kernel_size,
# #     padding="same",
# #     kernel_initializer=initializer,
# #     name="GFF_2",
# # )(GFF1)
# # print(GFF2.shape)

# # FDF = Add(name="FDF")([GFF2, F_m1])
# # print(FDF.shape)

# # FU = _UPN(F_m1)
# # print(FU.shape)

# # SR = Conv2D(
# #     3,
# #     kernel_size=kernel_size,
# #     padding="same",
# #     kernel_initializer=initializer,
# #     name="SR",
# # )(FU)
# # print(SR.shape)

# # rdn = Model(inputs=LR_input, outputs=SR)
# # rdn.compile(optimizer="adam", loss="mse", metrics=["accuracy"])
# # # rdn.summary()

# # history = rdn.fit(
# #     train_x_px, train_x, epochs=1000, validation_data=(val_x_px, val_x)
# # )
# # print(history)

# # predictions = rdn.predict(val_x_px)

# # print(predictions)


scale = 3  # LR缩小的比率
size = 32  # 输入大小，输出的size还要乘以scale
aug_num = 0  # 数据扩充的数量
num_G = 32  # RDN中每个卷积层卷积核的数量
Imgflag = 2  # 图片读取方式:0为RGB图，1为灰度图，2为亮度图中Y图层
if Imgflag == 0:
  channels = 3
else:
  channels = 1

Imglist = ['RGB', 'GRAY', 'YCrCb']
print('Have got parameters, for ' + Imglist[Imgflag] + ' images.')


import cv2
import os
import numpy as np
from tqdm import tqdm


def delcrust(HR_img, scale, Imgflag, Rflag=0):
  HR_size = HR_img.shape
  rem0 = HR_size[0]%scale
  rem1 = HR_size[1]%scale
  if Imgflag == 0:
    HR_img = HR_img[:HR_size[0]-rem0, :HR_size[1]-rem1, :]  # 裁掉无法被scale整除的边缘，即为处理后的HR
  else:
    HR_img = HR_img[:HR_size[0]-rem0, :HR_size[1]-rem1]  # 裁掉无法被scale整除的边缘，即为处理后的HR
  ''' Rflag=0，生成HR；Rflag=1，生成LR；Rflag=2，生成ILR。默认生成HR '''
  if Rflag == 0:
    return HR_img
  if Rflag == 1 or Rflag == 2:
    HR_size = HR_img.shape
    HR_size = (HR_size[1], HR_size[0])
    LR_size = (int(HR_size[0]/scale), int(HR_size[1]/scale))
    LR_img = cv2.resize(HR_img, LR_size, interpolation = cv2.INTER_LINEAR)  # 边长缩小scale倍
    if Rflag == 1:
      return LR_img
    else:
      ILR_img = cv2.resize(LR_img, HR_size, interpolation = cv2.INTER_LINEAR)  # 再插值恢复成HR的大小
      return ILR_img


def dataaug(img, size, flag):
  if flag == 0:
    img = cv2.flip(img, 0)  # 垂直翻转
  elif flag == 1:
    img = cv2.flip(img, 1)  # 水平翻转
  elif flag == 2:
    center = (size // 2, size // 2)
    img = cv2.warpAffine(img, cv2.getRotationMatrix2D(center, 90, 1), (size, size))  # 原图旋转90度
  elif flag == 3:
    center = (size // 2, size // 2)
    img = cv2.warpAffine(img, cv2.getRotationMatrix2D(center, 270, 1), (size, size))  # 原图旋转270度
  return img


def splitdata(img, size, Imgflag, aug_num=0):
  imglist = []
  length_num = img.shape[0]//size  # 长、宽分别可以分为多少块
  width_num = img.shape[1]//size
  for i in range(0, length_num):
    for j in range(0, width_num):
      if Imgflag == 0:
        img_piece = img[(0+i*size):(size+i*size), (0+j*size):(size+j*size), :]  # 分块
      else:
        img_piece = img[(0+i*size):(size+i*size), (0+j*size):(size+j*size)]  # 分块
      imglist.append(img_piece)
      for k in range(0, aug_num):  # aug_num最大为4
        imglist.append(dataaug(img_piece, size, k))
  imglist = np.array(imglist)
  return imglist


def gettraindata(folder_name_list, size, scale, Rflag=0, Imgflag=0, sizeflag=0, aug_num=0):
  firstflag = 0
  ''' Imgflag=0，读取RGB图；Imgflag=1，读取灰度图；Imgflag=2，读取亮度图Y图层。默认读取RGB '''
  if Imgflag == 0 or Imgflag == 2:
    Imgform = cv2.IMREAD_COLOR
  else:
    Imgform = cv2.IMREAD_GRAYSCALE
  for folder_name in folder_name_list:    
    for img_name in tqdm(os.listdir(folder_name)):  # os.listdir返回当前文件夹下所有文件
      file_name = folder_name + '/' + img_name
      img = cv2.imread(file_name, Imgform)  # 读取文件
      if Imgflag == 2:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2YCR_CB)  # 将RGB图像转换为YCrCb图像
        img = img[:, :, 0]  # 只取Y图层
      ''' Rflag=0，生成HR；Rflag=1，生成LR；Rflag=2，生成ILR '''
      img = delcrust(img, scale, Imgflag, Rflag)  # 按照scale大小裁剪无法被整除的边缘
      if (Rflag == 0 and sizeflag == 0) or (Rflag != 0 and sizeflag == 1):  # 对于HR默认是size*scale，其余默认是size。可以改变sizeflag来改变图像大小
        splitsize = size*scale
      else:
        splitsize = size
      sub_imglist = splitdata(img, splitsize, Imgflag, aug_num)
      if firstflag == 0:
        imglist = sub_imglist.copy()  # 第一张图要生成imglist
        firstflag += 1
      else:
        imglist = np.append(imglist, sub_imglist, axis=0)  # 其余的只要叠加在第一维度（图像数）就可以
  return imglist


train_folders = ['train']
val_folders = ['test']
y_train = gettraindata(train_folders, size, scale, Rflag=0, Imgflag=Imgflag, sizeflag=0, aug_num=aug_num)
x_train = gettraindata(train_folders, size, scale, Rflag=1, Imgflag=Imgflag, sizeflag=0, aug_num=aug_num)
y_val = gettraindata(val_folders, size, scale, Rflag=0, Imgflag=Imgflag, sizeflag=0)
x_val = gettraindata(val_folders, size, scale, Rflag=1, Imgflag=Imgflag, sizeflag=0)
if Imgflag != 0:  # 只有一个图层就需要单独增加层数这一维度
  x_train = np.reshape(x_train, (x_train.shape[0], x_train.shape[1], x_train.shape[2], 1))
  y_train = np.reshape(y_train, (y_train.shape[0], y_train.shape[1], y_train.shape[2], 1))
  x_val = np.reshape(x_val, (x_val.shape[0], x_val.shape[1], x_val.shape[2], 1))
  y_val = np.reshape(y_val, (y_val.shape[0], y_val.shape[1], y_val.shape[2], 1))
y_train = y_train/255.0  # 归一化
x_train = x_train/255.0
y_val = y_val/255.0
x_val = x_val/255.0

print('\n')
print(x_train.shape)
print(y_train.shape)
print(x_val.shape)
print(y_val.shape)


import tensorflow as tf
import numpy as np
from tensorflow.keras.layers import Conv2D
from tensorflow.keras import Model
from tensorflow.keras.initializers import he_normal
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import backend as K


class Subpixel(Conv2D):
    def __init__(self,
                 filters,
                 kernel_size,
                 r,
                 padding='valid',
                 data_format=None,
                 strides=(1,1),
                 activation=None,
                 use_bias=True,
                 kernel_initializer='glorot_uniform',
                 bias_initializer='zeros',
                 kernel_regularizer=None,
                 bias_regularizer=None,
                 activity_regularizer=None,
                 kernel_constraint=None,
                 bias_constraint=None,
                 **kwargs):
        super(Subpixel, self).__init__(
            filters=r*r*filters,
            kernel_size=kernel_size,
            strides=strides,
            padding=padding,
            data_format=data_format,
            activation=activation,
            use_bias=use_bias,
            kernel_initializer=kernel_initializer,
            bias_initializer=bias_initializer,
            kernel_regularizer=kernel_regularizer,
            bias_regularizer=bias_regularizer,
            activity_regularizer=activity_regularizer,
            kernel_constraint=kernel_constraint,
            bias_constraint=bias_constraint,
            **kwargs)
        self.r = r

    def _phase_shift(self, I):
        r = self.r
        bsize, a, b, c = I.get_shape().as_list()
        bsize = K.shape(I)[0] # Handling Dimension(None) type for undefined batch dim
        X = K.reshape(I, [bsize, a, b, int(c/(r*r)),r, r]) # bsize, a, b, c/(r*r), r, r
        X = K.permute_dimensions(X, (0, 1, 2, 5, 4, 3))  # bsize, a, b, r, r, c/(r*r)
        #Keras backend does not support tf.split, so in future versions this could be nicer
        X = [X[:,i,:,:,:,:] for i in range(a)] # a, [bsize, b, r, r, c/(r*r)
        X = K.concatenate(X, 2)  # bsize, b, a*r, r, c/(r*r)
        X = [X[:,i,:,:,:] for i in range(b)] # b, [bsize, r, r, c/(r*r)
        X = K.concatenate(X, 2)  # bsize, a*r, b*r, c/(r*r)
        return X

    def call(self, inputs):
        return self._phase_shift(super(Subpixel, self).call(inputs))

    def compute_output_shape(self, input_shape):
        unshifted = super(Subpixel, self).compute_output_shape(input_shape)
        return (unshifted[0], self.r*unshifted[1], self.r*unshifted[2], int(unshifted[3]/(self.r*self.r)))

    def get_config(self):
        config = super(Conv2D, self).get_config()
        config.pop('rank')
        config.pop('dilation_rate')
        config['filters']= int(config['filters'] / self.r*self.r)
        config['r'] = self.r
        return config


class ResidualDenseBlock(Model):
    def __init__(self, num_G):
        super(ResidualDenseBlock, self).__init__()
        self.num_G = num_G

        self.c1 = Conv2D(filters=num_G, kernel_initializer=he_normal(), kernel_size=(3, 3), activation='relu',
                         padding='same')
        self.c2 = Conv2D(filters=num_G, kernel_initializer=he_normal(), kernel_size=(3, 3), activation='relu',
                         padding='same')
        self.c3 = Conv2D(filters=num_G, kernel_initializer=he_normal(), kernel_size=(3, 3), activation='relu',
                         padding='same')
        self.c4 = Conv2D(filters=num_G, kernel_initializer=he_normal(), kernel_size=(3, 3), activation='relu',
                         padding='same')
        self.c5 = Conv2D(filters=num_G, kernel_initializer=he_normal(), kernel_size=(3, 3), activation='relu',
                         padding='same')
        self.c6 = Conv2D(filters=num_G, kernel_initializer=he_normal(), kernel_size=(3, 3), activation='relu',
                         padding='same')

        self.c = Conv2D(filters=64, kernel_initializer=he_normal(), kernel_size=(1, 1), padding='same')

    def call(self, inputs):
        x1 = self.c1(inputs)
        y1 = tf.concat([inputs, x1], 3)

        x2 = self.c2(y1)
        y2 = tf.concat([inputs, x1, x2], 3)

        x3 = self.c3(y2)
        y3 = tf.concat([inputs, x1, x2, x3], 3)

        x4 = self.c4(y3)
        y4 = tf.concat([inputs, x1, x2, x3, x4], 3)

        x5 = self.c5(y4)
        y5 = tf.concat([inputs, x1, x2, x3, x4, x5], 3)

        x6 = self.c6(y5)
        y6 = tf.concat([inputs, x1, x2, x3, x4, x5, x6], 3)

        y = self.c(y6)
        return y + inputs


class RDN(Model):
    def __init__(self, num_G, channels, scale):
        super(RDN, self).__init__()
        self.num_G = num_G
        self.channels = channels
        self.scale = scale

        self.SFE1 = Conv2D(filters=64, kernel_initializer=he_normal(), kernel_size=(3, 3), padding='same')
        self.SFE2 = Conv2D(filters=64, kernel_initializer=he_normal(), kernel_size=(3, 3), padding='same')

        self.RDB1 = ResidualDenseBlock(self.num_G)
        self.RDB2 = ResidualDenseBlock(self.num_G)
        self.RDB3 = ResidualDenseBlock(self.num_G)
        self.RDB4 = ResidualDenseBlock(self.num_G)
        self.RDB5 = ResidualDenseBlock(self.num_G)
        self.RDB6 = ResidualDenseBlock(self.num_G)
        self.RDB7 = ResidualDenseBlock(self.num_G)
        self.RDB8 = ResidualDenseBlock(self.num_G)
        self.RDB9 = ResidualDenseBlock(self.num_G)
        self.RDB10 = ResidualDenseBlock(self.num_G)
        self.RDB11 = ResidualDenseBlock(self.num_G)
        self.RDB12 = ResidualDenseBlock(self.num_G)
        self.RDB13 = ResidualDenseBlock(self.num_G)
        self.RDB14 = ResidualDenseBlock(self.num_G)
        self.RDB15 = ResidualDenseBlock(self.num_G)
        self.RDB16 = ResidualDenseBlock(self.num_G)
        self.RDB17 = ResidualDenseBlock(self.num_G)
        self.RDB18 = ResidualDenseBlock(self.num_G)
        self.RDB19 = ResidualDenseBlock(self.num_G)
        self.RDB20 = ResidualDenseBlock(self.num_G)

        self.GFF1 = Conv2D(filters=64, kernel_initializer=he_normal(), kernel_size=(1, 1), padding='same')
        self.GFF2 = Conv2D(filters=64, kernel_initializer=he_normal(), kernel_size=(3, 3), padding='same')

        self.UP = Subpixel(64, (3,3), r=scale, padding='same',activation='relu')

        self.c = Conv2D(filters=self.channels, kernel_initializer=he_normal(), kernel_size=(3, 3), padding='same')

    def call(self, inputs):
        sfe1 = self.SFE1(inputs)
        sfe2 = self.SFE2(sfe1)

        rdb1 = self.RDB1(sfe2)
        rdb2 = self.RDB2(rdb1)
        rdb3 = self.RDB3(rdb2)
        rdb4 = self.RDB4(rdb3)
        rdb5 = self.RDB5(rdb4)
        rdb6 = self.RDB6(rdb5)
        rdb7 = self.RDB7(rdb6)
        rdb8 = self.RDB8(rdb7)
        rdb9 = self.RDB9(rdb8)
        rdb10 = self.RDB10(rdb9)
        rdb11 = self.RDB11(rdb10)
        rdb12 = self.RDB12(rdb11)
        rdb13 = self.RDB13(rdb12)
        rdb14 = self.RDB14(rdb13)
        rdb15 = self.RDB15(rdb14)
        rdb16 = self.RDB16(rdb15)
        rdb17 = self.RDB17(rdb16)
        rdb18 = self.RDB18(rdb17)
        rdb19 = self.RDB19(rdb18)
        rdb20 = self.RDB20(rdb19)
        rdb = tf.concat([rdb1, rdb2, rdb3, rdb4, rdb5, rdb6, rdb7, rdb8, rdb9, rdb10,
                rdb11, rdb12, rdb13, rdb14, rdb15, rdb16, rdb17, rdb18, rdb19, rdb20], 3)

        gff1 = self.GFF1(rdb)
        gff2 = self.GFF2(gff1)
        dff = sfe1 + gff2

        up = self.UP(dff)

        y = self.c(up)
        return y


def L1_loss(y_true, y_pred):
    return K.sum(K.abs(y_true - y_pred))

def L1_mean_loss(y_true, y_pred):
    return K.mean(K.abs(y_true - y_pred))


model = RDN(num_G=num_G, channels=channels, scale=scale)
model.compile(optimizer=Adam(lr=1e-4, beta_1=0.9, beta_2=0.999, epsilon=1e-8), loss=L1_loss)
model.build((None, 32, 32, channels))
model.summary()

from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.callbacks import ReduceLROnPlateau

checkpoint_save_path = 'RDN.h5'
best_ckpt = ModelCheckpoint(filepath=checkpoint_save_path, save_weights_only=True, save_best_only=True)
reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.707, patience=2, verbose=1)
callback_list = [best_ckpt, reduce_lr]
model.fit(x_train, y_train, batch_size=16, epochs=100, validation_data=(x_val, y_val), callbacks=callback_list)